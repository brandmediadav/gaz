@extends('layouts.app')

@section('content')
<div class="container">
    <div style="margin: 0 auto;text-align: center;background: #ffffff;border: 1px solid #eee;padding: 15px;box-shadow: 0 3px 6px #00000016;color: black;border-radius: 25px;">
        <form action="{{ route('historique_fournisseur') }}" method="GET">
            Search :  
            <select style="padding:8px" class="btn-spanen">
                <option value="1">Bon Commande</option>
                <option value="1">Bon Livraison</option>
                <option value="1">Fournisseur</option>
                <option value="1">Camion</option>
                <option value="1">Client</option>
            </select>
            <input  type="search" class="btn-spanen" placeholder="id.." >
            <input  type="date" class="btn-spanen" name="date" >
            <button class="btn btn_search_small" onclick="fun()"> <i class="fas fa-search"></i> </button>
        </form>
        
    </div>

</div>
@endsection


@push('scripts')

    <script>
        function fun(){
            console.log('vlaue: ' + $('input[type="date"]').val());
        }
    </script>
@endpush
