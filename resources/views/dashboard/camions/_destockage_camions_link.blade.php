@extends('layouts.app')

@section('content')
<div class="container">
    <div class="page_links">
        <div class="row">
            <div class="col-md-6">
                <a href="{{ route('recap_stock_camions') }}" > <i class="fas fa-file-download"></i>  Recap Stock Camions  </a>
            </div>
            <div class="col-md-6">
                <a href="{{ route('stock_retour') }}" > <i class="fas fa-file-medical-alt"></i>  Stock retour  </a>
            </div>          
        </div>
    </div>
</div>
@endsection
