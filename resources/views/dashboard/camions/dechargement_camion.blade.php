@extends('layouts.app')

@section('content')
<div class="container">
    <table class="table table-bordered text-center">
        <thead>
             <tr>
                 <th>BOUTEILLES GAZ</th>
                 <th>REMPLIES</th>
                 <th>DEFECTUEUSES</th>
                 <th>CONSIGNE</th>
                 <th>ETRANGER</th>
             </tr>
        </thead>
        <tbody  style="background: #7cb3b9;">
         <tr>
             <td> <b>PROPANE 35kg</b> </td>
             <td><input type="text" class="btn-spanen"></td>
             <td><input type="text" class="btn-spanen"></td>
             <td><input type="text" class="btn-spanen"></td>
             <td><input type="text" class="btn-spanen"></td>
         </tr>
         <tr>
              <td><b>BUTANE &nbsp;&nbsp; 12kg</b></td>
              <td><input type="text" class="btn-spanen"></td>
              <td><input type="text" class="btn-spanen"></td>
              <td><input type="text" class="btn-spanen"></td>
              <td><input type="text" class="btn-spanen"></td>
          </tr>
          <tr>
              <td><b>BUTANE &nbsp;&nbsp;&nbsp; 6kg</b></td>
              <td><input type="text" class="btn-spanen"></td>
              <td><input type="text" class="btn-spanen"></td>
              <td><input type="text" class="btn-spanen"></td>
              <td><input type="text" class="btn-spanen"></td>
          </tr>
          <tr>
              <td><b>BUTANE &nbsp;&nbsp;&nbsp; 3kg</b></td>
              <td><input type="text" class="btn-spanen"></td>
              <td><input type="text" class="btn-spanen"></td>
              <td><input type="text" class="btn-spanen"></td>
              <td><input type="text" class="btn-spanen"></td>
          </tr>
        
        </tbody>
    </table>
    <br>
    <div class="row">
        <div class="col-md-6">
            <div class="text-left">
                <h5 class="mode_paiement_title" style="margin:0;margin-bottom:10px"> ENCAISSEMENTS </h5>
                <table>
                    <tr>
                        <td> <h5>Chéque </h5> </td>
                        <td><input type="text" placeholder="montant" class="btn-spanen" >
                        </td>
                    </tr>
                    <tr>
                        <td> <h5>Espece </h5> </td>
                        <td>
                            <input type="text" placeholder="montant" class="btn-spanen" >
                        </td>
                    </tr>
                    <tr>
                        <td> <h5>A terme </h5> </td>
                        <td><input type="text" placeholder="montant" class="btn-spanen" ></td>
                    </tr>
                    
                </table>
            </div>
        </div>
        <div class="col-md-6">
            <div class="text-left">
                <h5 class="mode_paiement_title" style="margin:0;margin-bottom:10px">Charges </h5>
                <table>
                    <tr>
                        <td> <h5>Frais </h5> </td>
                        <td><input type="text" placeholder="montant" class="btn-spanen" >
                        </td>
                    </tr>
                    <tr>
                        <td> <h5>Gazoil </h5> </td>
                        <td><input type="text" placeholder="montant" class="btn-spanen" >
                        </td>
                    </tr>
                   
                    
                </table>
                <button class="btn-imprimer"> <i class="fas fa-file-download"></i> Validé</button>

</div>
@endsection