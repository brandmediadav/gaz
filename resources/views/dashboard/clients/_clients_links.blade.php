@extends('layouts.app')

@section('content')
<div class="container">
    <div class="page_links">
        <div class="row">
            <div class="col-md-6">
                <a href="{{ route('liste_clients') }}" ><i class="fas fa-clipboard-list"></i> Liste Clients </a>
            </div>
            <div class="col-md-6">
                <a href="{{ route('ajouter_client') }}" > <i class="fas fa-user-plus"></i> Ajouter Client </a>
            </div>
            <div class="col-md-6">
                <a href="{{ route('client_debiteur') }}" > <i class="fas fa-file-medical-alt"></i> Client Débiteur </a>
            </div>
            <div class="col-md-6">
                <a href="{{ route('compte_terms_client') }}" > <i class="fas fa-file-medical-alt"></i> Compte à terms client </a>
            </div>
        </div>
    </div>
</div>
@endsection
