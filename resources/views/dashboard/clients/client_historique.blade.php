@extends('layouts.app')

@section('content')
<div class="container page-cleint-historique">
    <div class="row">
        <div class="col-md-4">
            <table>
                <tr class="mb-4">
                    <td style="padding: 10px 0;">Compte client </td>
                    <td> <span class="span_designed"><b>0775555</b></span> </td>
                </tr>
                <tr>
                    <td style="padding: 10px 0;">Raison Sociale </td>
                    <td> <span class="span_designed"><b>Ahmed Mohammed</b></span> </td>
                </tr>
               
                <tr>
                    <td style="padding: 10px 0;">Chiffre d'affaire </td>
                    <td> <span class="span_designed"><b>9022200,00 MAD</b></span></td>
                </tr>
                <tr>
                    <td colspan="2"><br><br><br></td>
                </tr>
            </table>
            <table >
                <tr>
                    <td style="padding: 10px 0;">Perte </td>
                    <td> <span class="span_designed"><b>5000,00 MAD</b></span></td>
                </tr>
                <tr>
                    <td style="padding: 10px 0;">Gain </td>
                    <td> <span class="span_designed"><b>30000,00 MAD</b></span></td>
                </tr>
                <tr>
                    <td style="padding: 10px 0;">Net </td>
                    <td> <span class="span_designed"><b>25000,00 MAD</b></span></td>
                </tr>
            </table>
        </div>
        <div class="col-md-4">
            <table class="table table-no-border table-date-filter">
                <tr>
                    <td>De</td>
                    <td><input type="date"></td>
                </tr>
                <tr>
                    <td>A</td>
                    <td>
                        <input type="date">
                        <button class="btn-search-filterr" type="button" > <i class="fas fa-search"></i> </button>
                    </td>
                </tr>
            </table> 
        </div>
        <div class="col-md-4"> 
            <div class="float-right">
                <table class="table table-bordered" style="max-width: 150px;">
                    <thead>
                        <tr>
                            <th>QT</th>
                            <th>Remplir</th>
                            <th>Vide</th>
                            <th>Défec</th>
                            <th>Autre</th>
                        </tr>
                    </thead>
                    <tr>
                        <td><b>3KG</b></td>
                        <td><span class="span_designed">1450</span></td>
                        <td><span class="span_designed">0</span></td>
                        <td><span class="span_designed">0</span></td>
                        <td><span class="span_designed">0</span></td>
                    </tr>
                    <tr>
                        <td><b>6KG</b></td>
                        <td><span class="span_designed">2250</span></td>
                        <td><span class="span_designed">0</span></td>
                        <td><span class="span_designed">0</span></td>
                        <td><span class="span_designed">0</span></td>
                    </tr>
                    <tr>
                        <td><b>12KG</b></td>
                        <td><span class="span_designed">165</span></td>
                        <td><span class="span_designed">0</span></td>
                        <td><span class="span_designed">0</span></td>
                        <td><span class="span_designed">0</span></td>
                    </tr>
                    <tr>
                        <td><b>35KG</b></td>
                        <td><span class="span_designed">0</span></td>
                        <td><span class="span_designed">0</span></td>
                        <td><span class="span_designed">225</span></td>
                        <td><span class="span_designed">0</span></td>
                    </tr>
                </table>
                <table>
                    <tr>
                        <td style="padding: 10px 0;">Quantité &nbsp;
                            <select class="btn-spanen">
                                <option value="1">tout</option>
                                <option value="1">35kg</option>
                                <option value="1">12kg</option>
                                <option value="1">6kg</option>
                                <option value="1">3kg</option>
                            </select>
                        </td>
                        <td> <span class="span_designed"><b>150000</b></span></td>
                    </tr>    
                </table>  
            </div>
        </div>
    </div>
    

    <div class="btn btn-lg btn-solde" style="top:440px">Solde <span>5004200,00 MAD</span></div>
    
    <table id="example" class="display dataTables_wrapper" style="width:100%">
        <thead>
            <tr>
                <th>Date</th>
                <th>Libellé</th>
                <th>Mode de Paiement</th>
                <th>QN Sortie</th>
                <th>QN Entre</th>
                <th>Débit</th>
                <th>Crédit</th>
            </tr>
        </thead>
        <tbody>
                <tr>
                    <td>02/12/2019</td>
                    <td>achat vide</td>
                    <td>Banque</td>
                    <td>200</td>
                    <td>150</td>
                    <td>20000,00</td>
                    <td><span class="badge badge-secondary">0</span></td>
                    
                </tr>
                <tr>
                    <td>05/11/2019</td>
                    <td>vente remplié</td>
                    <td>Banque</td>
                    <td>500</td>
                    <td>150</td>
                    <td><span class="badge badge-secondary">0</span></td>
                    <td>1000,00</td>
                </tr>
                <tr>
                    <td>02/12/2019</td>
                    <td>achat vide</td>
                    <td>Banque</td>
                    <td>200</td>
                    <td>150</td>
                    <td>20000,00</td>
                    <td><span class="badge badge-secondary">0</span></td>
                    
                </tr>
                <tr>
                    <td>05/11/2019</td>
                    <td>vente remplié</td>
                    <td>Banque</td>
                    <td>500</td>
                    <td>150</td>
                    <td><span class="badge badge-secondary">0</span></td>
                    <td>1000,00</td>
                </tr>
               
        </tbody>

    </table>

    <table cellspacing="0" cellpadding="0" border="0">
        <tbody>
            <tr>
                <td class="gutter">
                    <div class="line number1 index0 alt2" style="display: none;">1</div>
                </td>
                <td class="code">
                    <div class="container" style="display: none;">
                        <div class="line number1 index0 alt2" style="display: none;">&nbsp;</div>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
</div>
@endsection



@push('scripts')
    <script type="text/javascript">
        jQuery('#example').DataTable({
            "paging": false,
            "scrollY":  "480px",
            "scrollCollapse": true,

        });
    </script>
@endpush