@extends('layouts.app')

@section('content')
<div class="container">
    <div class="page_links fullwid">
        <div class="row">
            <div class="col-md-4">
                <a href="{{ route('tva') }}" > <i class="fas fa-file-download"></i>    TVA   </a>
            </div>
            <div class="col-md-4">
                <a href="{{ route('charge') }}" > <i class="fas fa-file-medical-alt"></i> Charge  </a>
            </div>
            <div class="col-md-4">
                <a href="{{ route('perte_gain') }}" > <i class="fas fa-file-medical-alt"></i>     Perte/ Gain     </a>
            </div>
            <div class="col-md-4">
                <a href="{{ route('caisse') }}" > <i class="fas fa-file-medical-alt"></i>    Caisse  </a>
            </div>
            <div class="col-md-4">
                <a href="{{ route('banque') }}" > <i class="fas fa-file-medical-alt"></i>     Banque     </a>
            </div>
            <div class="col-md-4">
                <a href="{{ route('cheque_eucaissement') }}" > <i class="fas fa-file-medical-alt"></i>     Cheque Eucaissement     </a>
            </div>
            <div class="col-md-4">
                <a href="{{ route('beneficenet') }}" > <i class="fas fa-file-medical-alt"></i> beneficenet </a>
            </div>
            <div class="col-md-4">
                <a href="{{ route('achat_de_boutielles') }}" > <i class="fas fa-file-medical-alt"></i> Achat de boutielles </a>
            </div>
            
            
           
           
        </div>
    </div>
</div>
@endsection
