@extends('layouts.app')

@section('content')
<div class="container">
    <div class="page_links">
        <div class="row">
            <div class="col-md-6">
                <a href="{{ route('saisie_chargement') }}" > <i class="fas fa-file-download"></i>   Saisie de Chargement  </a>
            </div>
            <div class="col-md-6">
                <a href="{{ route('bon_chargement') }}" > <i class="fas fa-vote-yea"></i>  Gestion des Camions </a>
            </div>
           
            <div class="col-md-6">
                <a href="{{ route('dechargement_camion') }}" > <i class="fas fa-truck-loading"></i>  Déchargement Camion  </a>
            </div>      
        </div>
    </div>
</div>
@endsection
