@extends('layouts.app')

@section('content')
<div class="container">

    <div style="max-width:800px;text-align:left">
        <table id="example" class="display dataTables_wrapper" style="width:100%;">
            <thead>
                <tr>
                    <th class="text-left">Camion</th>
                    <th>Responsable</th>
                    <th>Chiffre d'affaire</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                    <tr>
                        <td class="text-left">Camion</td>
                        <td>Ahmed Salam</td>
                        <td>20000,00 MAD</td>
                        <td>
                            <a href="{{ route('historique_camion') }}" class="btn btn-success btn-sm">Historique</a>
                            <a href="{{ route('bon_chargement_bon_chargement') }}" class="btn btn-primary btn-sm">Bon de Chargement</a>
                            <a href="#" class="btn btn-warning btn-sm" data-toggle="modal" data-target="#exampleModal">Stock Camion</a>
                        </td>
                        
                    </tr>
                    <tr>
                        <td class="text-left">Camion</td>
                        <td>Ahmed Salam</td>
                        <td>20000,00 MAD</td>
                        <td>
                            <a href="#" class="btn btn-success btn-sm">Historique</a>
                            <a href="#" class="btn btn-primary btn-sm">Bon de Chargement</a>
                            <a href="#" class="btn btn-warning btn-sm">Stock Camion</a>
                        </td>
                        
                    </tr>
                    
                    <tr>
                        <td class="text-left">Camion 4</td>
                        <td>Ahmed Salam</td>
                        <td>20000,00 MAD</td>
                        <td>
                            <a href="#" class="btn btn-success btn-sm">Historique</a>
                            <a href="#" class="btn btn-primary btn-sm">Bon de Chargement</a>
                            <a href="#" class="btn btn-warning btn-sm">Stock Camion</a>
                        </td>
                        
                    </tr>
                    
                    <tr>
                        <td class="text-left">Camion 3</td>
                        <td>Ahmed Salam</td>
                        <td>20000,00 MAD</td>
                        <td>
                            <a href="#" class="btn btn-success btn-sm">Historique</a>
                            <a href="#" class="btn btn-primary btn-sm">Bon de Chargement</a>
                            <a href="#" class="btn btn-warning btn-sm">Stock Camion</a>
                        </td>
                        
                    </tr>
                    
                    <tr>
                        <td class="text-left">Camion 5</td>
                        <td>Ahmed Salam</td>
                        <td>20000,00 MAD</td>
                        <td>
                            <a href="#" class="btn btn-success btn-sm">Historique</a>
                            <a href="#" class="btn btn-primary btn-sm">Bon de Chargement</a>
                            <a href="#" class="btn btn-warning btn-sm">Stock Camion</a>
                        </td>
                        
                    </tr>
               
                    
                   
            </tbody>
    
        </table>
    
        <table cellspacing="0" cellpadding="0" border="0">
            <tbody>
                <tr>
                    <td class="gutter">
                        <div class="line number1 index0 alt2" style="display: none;">1</div>
                    </td>
                    <td class="code">
                        <div class="container" style="display: none;">
                            <div class="line number1 index0 alt2" style="display: none;">&nbsp;</div>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>


<!--  Modal Client  -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">STOCK CAMION</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <table class="table table-bordered text-center">
                <thead>
                     <tr>
                         <th>BOUTEILLES GAZ</th>
                         <th>REMPLIES</th>
                         <th>DEFECTUEUSES</th>
                         <th>CONSIGNE</th>
                         <th>ETRANGER</th>
                     </tr>
                </thead>
                <tbody  style="background: #7cb3b9;">
                 <tr>
                     <td> <b>PROPANE 35kg</b> </td>
                     <td><input type="text" class="btn-spanen"></td>
                     <td><input type="text" class="btn-spanen"></td>
                     <td><input type="text" class="btn-spanen"></td>
                     <td><input type="text" class="btn-spanen"></td>
                 </tr>
                 <tr>
                      <td><b>BUTANE &nbsp;&nbsp; 12kg</b></td>
                      <td><input type="text" class="btn-spanen"></td>
                      <td><input type="text" class="btn-spanen"></td>
                      <td><input type="text" class="btn-spanen"></td>
                      <td><input type="text" class="btn-spanen"></td>
                  </tr>
                  <tr>
                      <td><b>BUTANE &nbsp;&nbsp;&nbsp; 6kg</b></td>
                      <td><input type="text" class="btn-spanen"></td>
                      <td><input type="text" class="btn-spanen"></td>
                      <td><input type="text" class="btn-spanen"></td>
                      <td><input type="text" class="btn-spanen"></td>
                  </tr>
                  <tr>
                      <td><b>BUTANE &nbsp;&nbsp;&nbsp; 3kg</b></td>
                      <td><input type="text" class="btn-spanen"></td>
                      <td><input type="text" class="btn-spanen"></td>
                      <td><input type="text" class="btn-spanen"></td>
                      <td><input type="text" class="btn-spanen"></td>
                  </tr>
                
                </tbody>
            </table>
            <br>
            <div class="row">
                <div class="col-md-6">
                    <div class="text-left">
                        <h5 class="mode_paiement_title" style="margin:0;margin-bottom:10px"> ENCAISSEMENTS </h5>
                        <table>
                            <tr>
                                <td> <h5>Chéque </h5> </td>
                                <td><input type="text" placeholder="montant" class="btn-spanen" >
                                </td>
                            </tr>
                            <tr>
                                <td> <h5>Espece </h5> </td>
                                <td>
                                    <input type="text" placeholder="montant" class="btn-spanen" >
                                </td>
                            </tr>
                            <tr>
                                <td> <h5>A terme </h5> </td>
                                <td><input type="text" placeholder="montant" class="btn-spanen" ></td>
                            </tr>
                            
                        </table>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="text-left">
                        <h5 class="mode_paiement_title" style="margin:0;margin-bottom:10px">Charges </h5>
                        <table>
                            <tr>
                                <td> <h5>Frais </h5> </td>
                                <td><input type="text" placeholder="montant" class="btn-spanen" >
                                </td>
                            </tr>
                            <tr>
                                <td> <h5>Gazoil </h5> </td>
                                <td><input type="text" placeholder="montant" class="btn-spanen" >
                                </td>
                            </tr>
                           
                            
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
        </div>
      </div>
    </div>
  </div>

@endsection


@push('scripts')
    <script type="text/javascript">
        jQuery('#example').DataTable({
            "paging": false,
            "scrollY":  "480px",
            "scrollCollapse": true,
            "searching":false
        });
    </script>
@endpush
