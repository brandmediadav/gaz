@extends('layouts.app')

@section('content')
<div class="container">
   <div class="bon-command-fournisseur">
       <h5 class="float-right"> <span>{{ date('D d-M-Y ') }} </span> </h5>
       <br>
       <div class="clearfix"></div>
       <h3 class="text-center">BON DE CHARGEMENT NUMERO : <input style="max-width: 201px;" class="btn-spanen" type="text"></h3>
       <br>
       
       <table class="table table-bordered text-center">
           <thead>
                <tr>
                    <th>BOUTEILLES DE GAZ</th>
                    <th>REMPLIES (GAZ) </th>
                    <th>CONSIGNE</th>
                    <th>PRIX</th>
                </tr>
           </thead>
           <tbody  style="background: #7cb3b9;">
            <tr>
                <td> PROPANE 35kg </td>
                <td><input type="text" class="btn-spanen"></td>
                <td><input type="text" class="btn-spanen"></td>
                <td><input type="text" class="btn-spanen" ></td>
            </tr>
            <tr>
                 <td>BUTANE &nbsp;&nbsp; 12kg</td>
                 <td><input type="text" class="btn-spanen"></td>
                 <td><input type="text" class="btn-spanen"></td>
                 <td><input type="text" class="btn-spanen" ></td>
             </tr>
             <tr>
                 <td>BUTANE &nbsp;&nbsp;&nbsp; 6kg</td>
                 <td><input type="text" class="btn-spanen"></td>
                 <td><input type="text" class="btn-spanen"></td>
                 <td><input type="text" class="btn-spanen" ></td>
             </tr>
             <tr>
                 <td>BUTANE &nbsp;&nbsp;&nbsp; 3kg</td>
                 <td><input type="text" class="btn-spanen"></td>
                 <td><input type="text" class="btn-spanen"></td>
                 <td><input type="text" class="btn-spanen" ></td>
             </tr>
           </tbody>
       </table>
    <br>       

   <div style="max-width:820px;margin:0 auto">
    <div class="row">
        <div class="col-md-6 text-left">
           
        </div>
        <div class="col-md-6">
            <div style="">
                <table class="table-no-border" style="margin:0px 0 0px auto;">
                    <tr>
                        <td style="padding: 10px 0;min-width: 140px"> <b>TOTAL HT</b> &nbsp;&nbsp;  </td>
                        <td> <span class="span_designed"><b>5000,00 MAD</b></span></td>
                    </tr>
                    <tr>
                        <td style="padding: 10px 0;"> <b>TVA (10%)</b> &nbsp;&nbsp;&nbsp; </td>
                        <td> <span class="span_designed"><b>30000,00 MAD</b></span></td>
                    </tr>
                    <tr>
                        <td style="padding: 10px 0;"> <b>TOTAL TTC</b> &nbsp;&nbsp; </td>
                        <td> <span class="span_designed"><b>25000,00 MAD</b></span></td>
                    </tr>
                </table>

                <button class="btn-imprimer"> <i class="fas fa-file-download"></i> Validé</button>

              </div>
        </div>
    </div>

   </div>
      

   </div>
</div>
@endsection


@push('scripts')
    <script type="text/javascript">
        jQuery('#example').DataTable({
            "paging": false,
            "scrollY":        "480px",
            "scrollCollapse": true,
        });
    </script>
   
@endpush