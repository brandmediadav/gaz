@extends('layouts.app')

@section('content')
<div class="container">
   <div class="bon-command-fournisseur saise-des-chargement">
       <h3 class="text-center">CHARGEMENT CAMION</h3>
       <div style="max-width:595px;margin:0 auto;margin-bottom:20px" class="text-left"> 
        <b>CAMION 1 : &nbsp;</b>
             <select class="btn-spanen"> 
                 <option>
                    CAMION 1
                 </option> 
                 <option>
                    CAMION 2
                  </option> 
                  <option>
                    CAMION 3
                 </option> 
                 <option>
                    CAMION 4
                  </option> 
             </select> 
     </div>
       <div class="row">
           <div class="col-md-12">
            <table class="table table-bordered text-center">
                <thead>
                     <tr>
                         <th>BOUTEILLES GAZ</th>
                         <th>REMPLIES</th>
                         <th>PRIX</th>
                     </tr>
                </thead>
                <tbody  style="background: #7cb3b9;">
                 <tr>
                     <td> <b>PROPANE 35kg</b> </td>
                     <td><input type="text" class="btn-spanen"></td>
                     <td><input type="text" class="btn-spanen" disabled></td>
                 </tr>
                 <tr>
                      <td><b>BUTANE &nbsp;&nbsp; 12kg</b></td>
                      <td><input type="text" class="btn-spanen"></td>
                      <td><input type="text" class="btn-spanen" disabled></td>
                  </tr>
                  <tr>
                      <td><b>BUTANE &nbsp;&nbsp;&nbsp; 6kg</b></td>
                      <td><input type="text" class="btn-spanen"></td>
                      <td><input type="text" class="btn-spanen" disabled></td>
                  </tr>
                  <tr>
                      <td><b>BUTANE &nbsp;&nbsp;&nbsp; 3kg</b></td>
                      <td><input type="text" class="btn-spanen"></td>
                      <td><input type="text" class="btn-spanen" disabled></td>
                  </tr>
                  
                </tbody>
            </table>
           </div>
                 </div>

    <br>
    <div class="row">
        <div class="col-md-12">
            <div >
                <table class="table-no-border" style="margin:0px 0 0px auto;">
                    <tr>
                        <td style="padding: 10px 0;min-width: 140px"> <b>TOTAL HT</b> &nbsp;&nbsp;  </td>
                        <td> <span class="span_designed"><b>5000,00 MAD</b></span></td>
                    </tr>
                    <tr>
                        <td style="padding: 10px 0;"> <b>TVA (10%)</b> &nbsp;&nbsp;&nbsp; </td>
                        <td> <span class="span_designed"><b>30000,00 MAD</b></span></td>
                    </tr>
                    <tr>
                        <td style="padding: 10px 0;"> <b>TOTAL TTC</b> &nbsp;&nbsp; </td>
                        <td> <span class="span_designed"><b>25000,00 MAD</b></span></td>
                    </tr>
                </table>

                <button class="btn-imprimer"> <i class="fas fa-file-download"></i> Validé</button>

              </div>
        </div>
    </div>

      

   </div>
</div>
@endsection


@push('scripts')
    <script type="text/javascript">
        jQuery('#example').DataTable({
            "paging": false,
            "scrollY":        "480px",
            "scrollCollapse": true,
        });
    </script>
@endpush