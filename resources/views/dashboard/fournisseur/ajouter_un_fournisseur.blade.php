@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-4">
            <form>
                <div class="form-group">
                    <label for="">Compte Fournisseur</label>
                    <input class="form-control" placeholder="Numero Compte">
                </div>
                <div class="form-group">
                    <label for="">Raison Sociale</label>
                    <input class="form-control" placeholder="Raison Sociale">
                </div>
                <div class="form-group">
                     <label for="">Contact</label>
                     <input class="form-control" placeholder="Nom & prenom">
                 </div>
                 <div class="form-group">
                     <label for="">Address</label>
                     <input class="form-control" placeholder="La Ville">
                 </div>
                 <div class="form-group">
                     <label for="">RC</label>
                     <input class="form-control" placeholder="RC">
                 </div>
                 <div class="form-group">
                     <label for="">Patente</label>
                     <input class="form-control" placeholder="Patente">
                 </div>
                 <div class="form-group">
                     <label for="">ICE</label>
                     <input class="form-control" placeholder="ICE">
                 </div>
                 <div class="form-group">
                    <input class="btn btn-success" type="submit" value="Ajouter">
                </div>
            </form>
          </div>
        
    </div>
</div>
@endsection
