@extends('layouts.app')

@section('content')
<div class="container page-cleint-historique">
    <div class="row">
      
        <div class="col-md-4">
            <table class="table table-no-border table-date-filter" style="margin-top:0">
                <tr>
                    <td>De</td>
                    <td><input type="date"></td>
                </tr>
                <tr>
                    <td>A</td>
                    <td>
                        <input type="date">
                        <button class="btn-search-filterr" type="button" > <i class="fas fa-search"></i> </button>
                    </td>
                </tr>
            </table> 
        </div>
      
    </div>
    


    <div class="btn btn-lg btn-solde" style="top: 162px;">Solde <span>5004200,00 MAD</span></div>
    <br>
    <table id="example" class="display dataTables_wrapper" style="width:100%">
        <thead>
            <tr>
                <th>Date</th>
                <th>Libellé</th>
                <th>Détails</th>
                <th>QN Sortie</th>
                <th>QN Entre</th>
                <th>Débit</th>
                <th>Crédit</th>
            </tr>
        </thead>
        <tbody>
                <tr>
                    <td>02/12/2019</td>
                    <td>achat vide</td>
                    <td>35kg</td>
                    <td class="border-right">200</td>
                    <td>200</td>
                    <td>20000,00</td>
                    <td><span class="badge badge-secondary">0</span></td>
                    
                </tr>
                <tr>
                    <td>05/11/2019</td>
                    <td>vente remplié</td>
                    <td>35kg</td>
                    <td class="border-right">500</td>
                    <td>200</td>
                    <td><span class="badge badge-secondary">0</span></td>
                    <td>1000,00</td>
                </tr>
                <tr>
                    <td>02/12/2019</td>
                    <td>achat vide</td>
                    <td>35kg</td>
                    <td class="border-right">200</td>
                    <td>200</td>
                    <td>20000,00</td>
                    <td><span class="badge badge-secondary">0</span></td>
                    
                </tr>
                <tr>
                    <td>05/11/2019</td>
                    <td>vente remplié</td>
                    <td>35kg</td>
                    <td class="border-right">500</td>
                    <td>200</td>
                    <td><span class="badge badge-secondary">0</span></td>
                    <td>1000,00</td>
                </tr>
               
        </tbody>

    </table>

    <table cellspacing="0" cellpadding="0" border="0">
        <tbody>
            <tr>
                <td class="gutter">
                    <div class="line number1 index0 alt2" style="display: none;">1</div>
                </td>
                <td class="code">
                    <div class="container" style="display: none;">
                        <div class="line number1 index0 alt2" style="display: none;">&nbsp;</div>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
</div>
@endsection



@push('scripts')
    <script type="text/javascript">
        jQuery('#example').DataTable({
            "paging": false,
            "scrollY":  "480px",
            "scrollCollapse": true,
            "searching":false
        });
    </script>
@endpush