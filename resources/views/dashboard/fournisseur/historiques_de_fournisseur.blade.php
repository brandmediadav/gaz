@extends('layouts.app')

@section('content')
<div class="container">

    <br>
    <div style="max-width:600px;text-align:left">
        <table id="example" class="display dataTables_wrapper" style="width:100%;">
            <thead>
                <tr>
                    <th class="text-left">Fournisseur</th>
                    <th>Chiffre d'affaire</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                    <tr>
                        <td class="text-left">Ziz Gaz</td>
                        <td>15000,00 MAD</td>
                        <td>
                            <a href="{{ route('historique_fournisseur') }}" class="btn btn-success btn-sm"> <i class="fas fa-eye"></i> Compte de Fournisseur</a>
                        </td>
                        
                    </tr>
                    <tr>
                        <td class="text-left">Salam Gaz</td>
                        <td>20000,00 MAD</td>
                        <td>
                            <a href="{{ route('historique_fournisseur') }}" class="btn btn-success btn-sm"> <i class="fas fa-eye"></i> Compte de Fournisseur</a>
                        </td>
                    </tr>
            </tbody>
    
        </table>
    
        <table cellspacing="0" cellpadding="0" border="0">
            <tbody>
                <tr>
                    <td class="gutter">
                        <div class="line number1 index0 alt2" style="display: none;">1</div>
                    </td>
                    <td class="code">
                        <div class="container" style="display: none;">
                            <div class="line number1 index0 alt2" style="display: none;">&nbsp;</div>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
@endsection


@push('scripts')
    <script type="text/javascript">
        jQuery('#example').DataTable({
            "paging": false,
            "scrollY":  "480px",
            "scrollCollapse": true,
            "searching":false
        });
    </script>
@endpush
