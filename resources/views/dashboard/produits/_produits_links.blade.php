@extends('layouts.app')

@section('content')
<div class="container">
    <div class="page_links fullwid">
        <div class="row">
            <div class="col-md-4">
                <a href="{{ route('bouteil_vente_achat_particulier') }}" > <i class="fas fa-file-download"></i>   Bouteil Vente / Achat Particulier  </a>
            </div>           
        </div>
    </div>
</div>
@endsection
