@extends('layouts.app')

@section('content')
<div class="container">
   <div class="bon-command-fournisseur">
      
       <div class="mb-4 text-left">
            <p>Particulier</p>
            <p>Facture &nbsp;  <span style="background: #fff;padding: 6px 15px;box-shadow: 0 3px 6px inset #00000016;border-radius: 14px"><b>70000</b></span> </p>
       </div>
     </div>
       <div class="row">
           <div class="col-md-12">
            <br>
            <table class="table table-bordered text-center">
                <thead>
                     <tr>
                         <th>BOUTEILLES </th>
                         <th>REMPLIES</th>
                         <th>VIDE</th>
                         <th>PRIX</th>
                     </tr>
                </thead>
                <tbody  style="background: #7cb3b9;">
                 <tr>
                     <td> <b>PROPANE 35kg</b> </td>
                     <td><input type="text" class="btn-spanen"></td>
                     <td><input type="text" class="btn-spanen"></td>
                     <td><input type="text" class="btn-spanen" disabled></td>
                 </tr>
                 <tr>
                      <td><b>BUTANE &nbsp;&nbsp; 12kg</b></td>
                      <td><input type="text" class="btn-spanen"></td>
                      <td><input type="text" class="btn-spanen"></td>
                      <td><input type="text" class="btn-spanen" disabled></td>
                  </tr>
                  <tr>
                      <td><b>BUTANE &nbsp;&nbsp;&nbsp; 6kg</b></td>
                      <td><input type="text" class="btn-spanen"></td>
                      <td><input type="text" class="btn-spanen"></td>
                      <td><input type="text" class="btn-spanen" disabled></td>
                  </tr>
                  <tr>
                      <td><b>BUTANE &nbsp;&nbsp;&nbsp; 3kg</b></td>
                      <td><input type="text" class="btn-spanen"></td>
                      <td><input type="text" class="btn-spanen"></td>
                      <td><input type="text" class="btn-spanen" disabled></td>
                  </tr>
                </tbody>
            </table>
           </div>
           <div class="col-md-12">
               <br>
               <h5>Achat Vide : </h5>
            <table class="table table-bordered text-center">
                <thead>
                    <tr>
                        <th>Nom Marque </th>
                        <th>Quantité</th>
                        <th>PRIX</th>
                    </tr>
                </thead>
                <tbody  style="background: #7cb3b9;">
                    <tr>
                        <td><input type="text" class="btn-spanen"></td>
                        <td><input type="text" class="btn-spanen"></td>
                        <td><input type="text" class="btn-spanen"></td>
                    </tr>
                   
                </tbody>
            </table>
            </div>
       </div>

    <br>
    <div class="row">
        <div class="col-md-6 text-left">
            <div class="text-left">
                <h5 class="mode_paiement_title">Mode de Paiement </h5>
                <table>
                    <tr>
                        <td> <h5>Chéque </h5> </td>
                        <td><input type="text" placeholder="montant" class="btn-spanen" >
                            <input type="text" placeholder="numéro de chéque" class="btn-spanen" >
                        </td>
                    </tr>
                    <tr>
                        <td> <h5>Virement </h5> </td>
                        <td>
                            <input type="text" placeholder="virement" class="btn-spanen" >
                        </td>
                    </tr>
                    <tr>
                        <td> <h5>A terme </h5> </td>
                        <td><input type="text" placeholder="montant" class="btn-spanen" ></td>
                    </tr>
                    
                </table>
            </div>
        </div>
        <div class="col-md-6">
            <div >
                <table class="table-no-border" style="margin:0px 0 0px auto;">
                    <tr>
                        <td style="padding: 10px 0;min-width: 140px"> <b>TOTAL HT</b> &nbsp;&nbsp;  </td>
                        <td> <span class="span_designed"><b>5000,00 MAD</b></span></td>
                    </tr>
                    <tr>
                        <td style="padding: 10px 0;"> <b>TVA (10%)</b> &nbsp;&nbsp;&nbsp; </td>
                        <td> <span class="span_designed"><b>30000,00 MAD</b></span></td>
                    </tr>
                    <tr>
                        <td style="padding: 10px 0;"> <b>TOTAL TTC</b> &nbsp;&nbsp; </td>
                        <td> <span class="span_designed"><b>25000,00 MAD</b></span></td>
                    </tr>
                </table>

                <button class="btn-imprimer"> <i class="fas fa-file-download"></i> Validé</button>

              </div>
        </div>
    </div>

      

   </div>
</div>
@endsection


@push('scripts')
    <script type="text/javascript">
        jQuery('#example').DataTable({
            "paging": false,
            "scrollY":        "480px",
            "scrollCollapse": true,
        });
    </script>
@endpush