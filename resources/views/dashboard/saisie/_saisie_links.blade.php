@extends('layouts.app')

@section('content')
<div class="container">
    <div class="page_links fullwid">
        <div class="row">
            <div class="col-md-4">
                <a href="{{ route('saisie_achat_produits') }}" > <i class="fas fa-file-download"></i>   Saisie Achat produits  </a>
            </div>
            <div class="col-md-4">
                <a href="{{ route('saisie_vente_produits') }}" > <i class="fas fa-file-medical-alt"></i>  Saisie Vente produits  </a>
            </div>
            <div class="col-md-4">
                <a href="{{ route('saisie_charge') }}" > <i class="fas fa-file-medical-alt"></i>   Saisie de charge </a>
            </div>
            <div class="col-md-4">
                <a href="{{ route('paiement_creance') }}" > <i class="fas fa-file-medical-alt"></i>   Paiement Créance </a>
            </div>
            <div class="col-md-4">
                <a href="{{ route('tabeleau_des_prix') }}" > <i class="fas fa-file-medical-alt"></i>   Tabeleau des prix  </a>
            </div>
        </div>
    </div>
</div>
@endsection
