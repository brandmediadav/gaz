@extends('layouts.app')

@section('content')
<div class="container">

    <table class="table table-bordered text-center table-dynamic-rows table-date-filter">
        <thead>
            <tr>
                <th colspan="3">Painement Créance</th>
                
            </tr>
            
        </thead>
        <tbody  style="background: #7cb3b9;">
            <tr>
               
                <td colspan="2"><input style="max-width: 100%;min-width: 81%;" type="text" class="btn-spanen" placeholder="Facture"></td>
                <td><input  type="text" class="btn-spanen" placeholder="TOTAL" style="max-width: 200px"></td>
            </tr>
            <tr>
                <td>
                    <select class="btn-spanen"  style="max-width: 200px">
                        <option value="1" selected disabled>Choisé compte fournisseur</option>
                        <option value="1">G fournisseur 1</option>
                        <option value="1">Z fournisseur 2</option>
                        <option value="1">RT fournisseur 3</option>
                        <option value="1">GUD fournisseur 4</option>
                    </select>
                </td>
                <td><input type="text" class="btn-spanen" placeholder="Montant" style="max-width: 200px"></td>
                <td><input type="text" class="btn-spanen" placeholder="TVA" style="max-width: 200px"></td>
            </tr>
        </tbody>
    </table>

   <br>
    <div class="text-left">
        <h5 class="mode_paiement_title">Mode de Paiement </h5>
        <table>
            <tr>
                <td> <h5>Caisse </h5> </td>
                <td><input type="text" placeholder="montant" class="btn-spanen" >
                    <input type="text" placeholder="alémentation" class="btn-spanen" >
                </td>
            </tr>
            <tr>
                <td> <h5>Banque </h5> </td>
                <td>
                    <input type="text" placeholder="montant" class="btn-spanen" >
                    <input type="text" placeholder="numero" class="btn-spanen" >
                </td>
            </tr>
            <tr>
                <td> <h5>A terme </h5> </td>
                <td><input type="text" placeholder="montant" class="btn-spanen" >
                    <input type="text" placeholder="Echéauce" class="btn-spanen" ></td>
            </tr>
            
        </table>
    </div>
    <button class="btn-imprimer"> <i class="fas fa-file-download"></i> Validé</button>

</div>

<!--  Modal Client  -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ajouter un produit</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <table class="table-bordered table table-no-border client-details">
              <tr>
                  <td>Nom Produit</td>
                  <td><input class="span_designed"> </td>
              </tr>
              <tr>
                <td>Prix Produit</td>
                <td><input class="span_designed"> </td>
              </tr>
            
          </table>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-success" data-dismiss="modal"> <i class="fa fa-plus"></i> Ajouter </button>
        </div>
      </div>
    </div>
  </div>

@endsection


@push('scripts')

    <script>
        function list_produits(e){
            if ($(e.target).val() === 'ajouter_nouvelle') {
                $('#myModal').modal('show');
            }
        }
    </script>
@endpush
