@extends('layouts.app')

@section('content')
<div class="container">
   <div class="bon-command-fournisseur saise-des-chargement">
       <h3 class="text-center">Tableau des prix</h3>
       <div style="max-width:800px;margin:0 auto;margin-bottom:20px" class="text-left"> 
      <br>
       <div class="row">
           <div class="col-md-12">
            <table class="table table-bordered text-center">
                <thead>
                     <tr>
                         <th>Product</th>
                         <th>Prix Achat</th>
                         <th>Prix Achat Moyenne</th>
                         <th>Prix Vente</th>
                         <th>TVA</th>
                     </tr>
                </thead>
                <tbody  style="background: #7cb3b9;">
                 <tr>
                     <td> <b>PROPANE 35kg</b> </td>
                     <td><input type="text" class="btn-spanen"></td>
                     <td><input type="text" class="btn-spanen"></td>
                     <td><input type="text" class="btn-spanen"></td>
                     <td><input type="text" class="btn-spanen" disabled></td>
                 </tr>
                 <tr>
                      <td><b>BUTANE &nbsp;&nbsp; 12kg</b></td>
                      <td><input type="text" class="btn-spanen"></td> 
                      <td><input type="text" class="btn-spanen"></td>
                      <td><input type="text" class="btn-spanen"></td>
                      <td><input type="text" class="btn-spanen" disabled></td>
                  </tr>
                  <tr>
                      <td><b>BUTANE &nbsp;&nbsp;&nbsp; 6kg</b></td>
                      <td><input type="text" class="btn-spanen"></td> 
                       <td><input type="text" class="btn-spanen"></td>
                        <td><input type="text" class="btn-spanen"></td>                 
                      <td><input type="text" class="btn-spanen" disabled></td>
                  </tr>
                  <tr>
                      <td><b>BUTANE &nbsp;&nbsp;&nbsp; 3kg</b></td>
                      <td><input type="text" class="btn-spanen"></td>
                       <td><input type="text" class="btn-spanen"></td>
                        <td><input type="text" class="btn-spanen"></td>
                      <td><input type="text" class="btn-spanen" disabled></td>
                  </tr>
                  
                </tbody>
            </table>
            <button class="btn-imprimer"> <i class="fas fa-file-download"></i> Enregister</button>

           </div>
                 </div>
   
                </div>

      

   </div>
</div>
@endsection


@push('scripts')
   
@endpush