@extends('layouts.app')

@section('content')
<div class="container">
    <div class="page_links">
        <div class="row">
            <div class="col-md-6">
                <a href="{{ route('stock_depot') }}" > <i class="fas fa-file-download"></i> Stocks dépot  </a>
            </div>
            <div class="col-md-6">
                <a href="{{ route('stock_autre_produit') }}" > <i class="fas fa-file-medical-alt"></i> Stocks Autre Produit </a>
            </div>
        </div>
    </div>
</div>
@endsection
