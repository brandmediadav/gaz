<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <link rel="icon" type="image/png" href="{{ asset('/images/logo-gaz.png') }}" />
    <link rel="apple-touch-icon" type="image/png" href="{{ asset('/images/logo-gaz.png') }}" />
    <!-- bootstrap -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap/bootstrap.css') }}">
    <!-- app.js -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset('css/datatable.min.css') }}">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    
</head>
<body >
    
    <!-- 
    <div id="contentloading">
        <div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div>
    </div>
    -->

    <div class="adm">
        <!-- header --->
    <header class="admin-header">
        <div class="left-part">
            <a href="#" class="logo">
                <span class="logo-mini"></span>
                <span class="logo-lg"><b>Gaz </b> Control Panel </span>
            </a>
        </div>
        <div class="right-part">
            <a href="#" class="float-left to_small_sidebar" id="to_small_sidebar" data-toggle="push-menu" role="button">
                <i class="fa fa-bars" aria-hidden="true"></i>
            </a>
            
            <div class="dropdown float-right">
                <a role="button" class="dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown"
                    aria-haspopup="true" aria-expanded="false">
                     <img src="{{ asset('/images/logo-gaz.png') }}"
                        style="width:25px;border-radius:50%;margin-left: 5px;">
                        Asmaa Karim
                </a>
                <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                    <a class="dropdown-item" href="#">paramètres  </a>
                    <a class="dropdown-item" href="#">mot de passe</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#">Se déconnecter</a>
                </div>
            </div>
            <div class="dateete">
                @php setlocale(LC_TIME, "fr");@endphp
                {{  Carbon\Carbon::now()->formatLocalized('%d %B %Y') }}
            </div>
            
            
        </div>
    </header>


    <section class="main-sidebar">
        <div class="container">
            <div class="user_panel">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="info">
                            <label>Asmaa Karim</label>
                            <label class="online"> <i class="fa fa-circle text-success"></i> online </label>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Start Navigation -->
            <ul class="sidebar-menu tree">
               <li class="header">Navigation</li>

                <li class="active">
                    <a href="{{ route('home') }}" class="btn bg-gray   text-left">
                        <i class="fa fa-home"></i> <span>Accuiel</span> 
                    </a>
                </li>

                <!--  Fournisseur --->
                <li>
                    <a href="{{ route('fournisseur_links') }}">
                        <i class="fas fa-truck"></i> <span> Fournisseur </span> 
                    </a>
                </li>

                <!--  Chargement --->
                <li>
                    <a href="{{ route('distributeur_links') }}">
                        <i class="fas fa-truck-moving"></i>
                        <span>Distributeur </span> 
                    </a>
                </li>
                
             

                 <!--  Saisie des produits --->
                <li>
                    <a href="{{ route('produits_links') }}">
                        <i class="fas fa-tags"></i>
                        <span> Produits </span> 
                    </a>
                </li>

                <!--  Saisie des produits --->
                 <li>
                    <a href="{{ route('saisie_links') }}">
                        <i class="fas fa-marker"></i>
                        <span> Saisie </span> 
                    </a>
                </li>


                <!-- Gestion des clients  -->
                <li>
                    <a href="{{ route('clients_links') }}">
                        <i class="fas fa-user-tag"></i>
                        <span> Gestion des clients </span> 
                    </a>
                </li>

                <!-- Gestion des comptes interne  -->
                <li>
                    <a href="{{ route('_comptes_links') }}">
                        <i class="fas fa-user-circle"></i>
                        <span> Gestion des comptes </span> 
                    </a>
                </li>

                
                <!--  Gestion des stocks --->
                <li>
                    <a href="{{ route('stock_links') }}">
                        <i class="fas fa-cloud"></i>
                        <span> Gestion des stocks </span> 
                    </a>
                </li>

                <!-- Track GPS  -->
                <li>
                    <a href="{{ route('track_gps') }}" class="btn bg-gray   text-left">
                        <i class="fas fa-map-marked-alt"></i> <span>GPS Track </span> 
                    </a>
                </li>
               
                <li><br></li>
            </ul>
        </div>
    </section>

    <main class="py-4 content_section" id="my-app" >
        @include('layouts._session')
        @yield('content')
    </main>


    </div>
    
    <!-- jqeury -->
    <script src="{{ asset('js/jquery/jquery.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/jquery/popper.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/bootstrap/bootstrap.min.js') }}"  type="text/javascript"></script>
    <script src="{{ asset('js/datatable.min.js') }}" type="text/javascript"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="{{ asset('js/main_o.js') }}"></script>
    @stack('scripts')
</body>
</html>
