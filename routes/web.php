<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Auth::routes(['register' => false]);

Route::get('/', 'HomeController@index')->name('home');

// start fournisseur
Route::get('fournisseur',function(){
    return view('dashboard.fournisseur._fournisseur_links');
})->name('fournisseur_links');
Route::get('fournisseur/bon-commande-fournisseur',function(){
    return view('dashboard.fournisseur.bon_commande_fournisseur');
})->name('bon_commande_fournisseur');
Route::get('fournisseur/bon-livraison-fournisseur',function(){
    return view('dashboard.fournisseur.bon_livraison_fournisseur');
})->name('bon_livraison_fournisseur');
Route::get('fournisseur/historiques-de-fournisseur',function(){
    return view('dashboard.fournisseur.historiques_de_fournisseur');
})->name('historiques_de_fournisseur');
Route::get('fournisseur/ajouter-un-fournisseur',function(){
    return view('dashboard.fournisseur.ajouter_un_fournisseur');
})->name('ajouter_un_fournisseur');
Route::get('fournisseur/compte-terme-fournisseur',function(){
    return view('dashboard.fournisseur.compte_terme_fournisseur');
})->name('compte_terme_fournisseur');
Route::get('fournisseur/historiques-de-fournisseur/1',function(){
    return view('dashboard.fournisseur.historique_fournisseur');
})->name('historique_fournisseur');

// start distributeur
Route::get('distributeur',function(){
    return view('dashboard.distributeur._distributeur_links');
})->name('distributeur_links');

Route::get('distributeur/saisie-de-chargement',function(){
    return view('dashboard.distributeur.saisie_chargement');
})->name('saisie_chargement');

Route::get('distributeur/bon-de-chargement',function(){
    return view('dashboard.distributeur.bon_chargement');
})->name('bon_chargement');

Route::get('distributeur/dechargement-camion',function(){
    return view('dashboard.camions.dechargement_camion');
})->name('dechargement_camion');
Route::get('distributeur/bon-de-chargement/1',function(){
    return view('dashboard.distributeur.bon_chargement_bon_chargement');
})->name('bon_chargement_bon_chargement');

Route::get('distributeur/bon-de-chargement/historique/1',function(){
    return view('dashboard.distributeur.historique_camion');
})->name('historique_camion');

// saisie 
Route::get('saisie',function(){
    return view('dashboard.saisie._saisie_links');
})->name('saisie_links');
Route::get('saisie/saisie-achat-produits',function(){
    return view('dashboard.saisie.saisie_achat_produits');
})->name('saisie_achat_produits');
Route::get('saisie/saisie-vente-produits',function(){
    return view('dashboard.saisie.saisie_vente_produits');
})->name('saisie_vente_produits');
Route::get('saisie/saisie-charge',function(){
    return view('dashboard.saisie.saisie_charge');
})->name('saisie_charge');
Route::get('saisie/paiement-creance',function(){
    return view('dashboard.saisie.paiement_creance');
})->name('paiement_creance');
Route::get('saisie/tabeleau-des-prix',function(){
    return view('dashboard.saisie.tabeleau_des_prix');
})->name('tabeleau_des_prix');

//  produits
Route::get('produits',function(){
    return view('dashboard.produits._produits_links');
})->name('produits_links');
Route::get('produits/bouteil-vente-achat-particulier',function(){
    return view('dashboard.produits.bouteil_vente_achat_particulier');
})->name('bouteil_vente_achat_particulier');

// Gestion des stock
Route::get('gestion-des-stocks',function(){
    return view('dashboard.stock._stock_links');
})->name('stock_links');
Route::get('gestion-des-stocks/stock-depot',function(){
    return view('dashboard.stock.stock_depot');
})->name('stock_depot');
Route::get('gestion-des-stocks/stock-autre-produit',function(){
    return view('dashboard.stock.stock_autre_produit');
})->name('stock_autre_produit');

// Gestion des clients
Route::get('gestion-des-clients',function(){
    return view('dashboard.clients._clients_links');
})->name('clients_links');
Route::get('gestion-des-clients/liste-clients',function(){
    return view('dashboard.clients.liste_clients');
})->name('liste_clients');
Route::get('gestion-des-clients/ajouter-client',function(){
    return view('dashboard.clients.ajouter_client');
})->name('ajouter_client');
Route::get('gestion-des-clients/client-debiteur',function(){
    return view('dashboard.clients.client_debiteur');
})->name('client_debiteur');
Route::get('gestion-des-clients/compte-terms-client',function(){
    return view('dashboard.clients.compte_terms_client');
})->name('compte_terms_client');
Route::get('gestion-des-clients/client-historique/1',function(){
    return view('dashboard.clients.client_historique');
})->name('client_historique');

// Gestion des comptes 
Route::get('gestion-des-comptes',function(){
    return view('dashboard.comptes._comptes_links');
})->name('_comptes_links');
Route::get('gestion-des-comptes/tva',function(){
    return view('dashboard.comptes.tva');
})->name('tva');
Route::get('gestion-des-comptes/charge',function(){
    return view('dashboard.comptes.charge');
})->name('charge');
Route::get('gestion-des-comptes/boiteille-gaz',function(){
    return view('dashboard.comptes.boiteille_gaz');
})->name('boiteille_gaz');
Route::get('gestion-des-comptes/perte-gain',function(){
    return view('dashboard.comptes.perte_gain');
})->name('perte_gain');
Route::get('gestion-des-comptes/caisse',function(){
    return view('dashboard.comptes.caisse');
})->name('caisse');
Route::get('gestion-des-comptes/banque',function(){
    return view('dashboard.comptes.banque');
})->name('banque');
Route::get('gestion-des-comptes/cheque-eucaissement',function(){
    return view('dashboard.comptes.cheque_eucaissement');
})->name('cheque_eucaissement');
Route::get('gestion-des-comptes/beneficenet',function(){
    return view('dashboard.comptes.beneficenet');
})->name('beneficenet');
Route::get('gestion-des-comptes/achat-de-boutielles',function(){
    return view('dashboard.comptes.achat_de_boutielles');
})->name('achat_de_boutielles');


//  GPS  
Route::get('track-gps',function(){
    return view('dashboard.GPS.track_gps');
})->name('track_gps');

